# Testing For the UPennClearPassAPI

import UPennClearPassAPI
import sys
from os import getenv

fake_endpoint = "ee:ee:ee:ee:ee:ee"
test_endpoint = "ff:ff:ff:ff:ff:ff"

unpriv_username = getenv("CLEARPASSAPI_UNPRIV_USERNAME")
unpriv_secret = getenv("CLEARPASSAPI_UNPRIV_SECRET")
grant_type = "client_credentials"
server = getenv("CLEARPASSAPI_TEST_SERVER")
ca_certs = "test_files/upenn-ca-chain.pem"
# Create a bad connection and make sure it fails out

try:
    conn = UPennClearPassAPI.Connection(
        server, grant_type, unpriv_username, grant_type, ca_certs
    )
    print("Bad Creditials Fails.")
    sys.exit(1)
except UPennClearPassAPI.AccessTokenError:
    print("Bad Creditials Passes")


# Create a unpriv connection and try to do something

conn = UPennClearPassAPI.Connection(
    server, grant_type, unpriv_username, unpriv_secret, ca_certs
)

endpoints = UPennClearPassAPI.Endpoints(conn)

try:
    endpoints.get("aa:bb:cc:dd:ee:ff")
    print("Unauthorized Test Fails: Able to get endpoint with unpriv account")
    sys.exit(1)
except UPennClearPassAPI.UnauthorizedError:
    print("Unauthorized Test Passes")


# cleanup
del conn
del endpoints


priv_username = getenv("CLEARPASSAPI_PRIV_USERNAME")
priv_secret = getenv("CLEARPASSAPI_PRIV_SECRET")

conn = UPennClearPassAPI.Connection(
    server, grant_type, priv_username, priv_secret, ca_certs
)

endpoints = UPennClearPassAPI.Endpoints(conn)

# try and get a non-existant endpoint
try:
    endpoints.get(test_endpoint)
    print("Get Non-Existant Endpoint Fail: something was returned")
    sys.exit(1)
except UPennClearPassAPI.UnknownEndpointError:
    print("Get Non-Existant Endpoint Passes")


# add endpoint
try:
    endpoints.add("ff:ff:ff:ff:ff:ff")
    print("Adding Test Endpoint passes")
except Exception as e:
    print(f"Adding Test Endpoint Fails: {e}")
    sys.exit(1)

# try and add it again; expect a failure
try:
    endpoints.add("ff:ff:ff:ff:ff:ff")
    print("Duplicate MAC Address Add Fail: Able add duplicate endpoint")
    sys.exit(1)
except UPennClearPassAPI.DuplicateEndpointError:
    print("Duplicate MAC Address Add Passes")
except Exception as e:
    print(f"Unknown error while adding duplicate MAC address : {e}")
    sys.exit(1)

# Try and disable the endpoint
try:
    endpoints.disable(test_endpoint, "This is a test", "stupid ref")
    endpoint = endpoints.get(test_endpoint)

    # Check the data is correct
    if endpoint["status"] != "Disabled":
        print("Failed to disable test endpoint: Status Field not correct")
        sys.exit(1)
    print("Disabling test endpoint Passes")
except Exception as e:
    print(f"Disabling test endpoint fails: {e}")
    sys.exit(1)

# try and disable none existant endpoint
try:
    endpoints.disable(fake_endpoint, "This is a test", "stupid ref")
    print(
        "Disabling fake endpoint fails: Non-existing Endpoint exists. Check ClearPass"
    )
    sys.exit(1)
except UPennClearPassAPI.UnknownEndpointError:
    print(f"Disabling Fake Endpoint Passes")
except Exception as e:
    print(f"Unknow error disabling fake endpoint: {e}")
    sys.exit(1)

# Try and enable the endpoint
try:
    endpoints.enable(test_endpoint)
    endpoint = endpoints.get(test_endpoint)

    # Check the data is correct
    if endpoint["status"] != "Unknown":
        print("Failed to enable test endpoint: Status Field not correct")
        sys.exit(1)
    print("Enabling test endpoint Passes")
except Exception as e:
    print(f"Enabling test endpoint fails: {e}")
    sys.exit(1)

# try and disable none existant endpoint
try:
    endpoints.enable(fake_endpoint)
    print("Enabling fake endpoint fails: Non-existing Endpoint exists. Check ClearPass")
    sys.exit(1)
except UPennClearPassAPI.UnknownEndpointError:
    print(f"Enabling Fake Endpoint Passes")
except Exception as e:
    print(f"Unknown error enabling fake endpoint: {e}")
    sys.exit(1)

# try and delete the test endpoint
try:
    endpoints.delete(test_endpoint)
    print("Delete test endpoint passes")
except Exception as e:
    print(f"Delete test endpoint fails: {e}")
    sys.exit(1)

# try and delete the fake endpoint
try:
    endpoints.delete(fake_endpoint)
    print("Delete Fake endpoint fails: able to delete fake endpoint")
except UPennClearPassAPI.UnknownEndpointError:
    print(f"Delete Fake Endpoint Passes")
except Exception as e:
    print(f"delete fake endpoint fails: {e}")
    sys.exit(1)
