UPennClearPassAPI
=================

This is a wrapper library around Aruba's ClearPass API. It is designed to make
it easier to interact with UPenn's ClearPass Servers. It's quite functional
for other ClearPass installations, but has some UPenn business logic bulit
into it to ensure different attributes are properly set.


Requirements Modules
--------------------
This library uses the following Python modules:

* json
* urllib3
* arrow

The test script uses the following Python modules in addition to the ones
above:

* os
* sys



Establishing a "Connection" with ClearPass
------------------------------------------
When using the UPennClearPass API, you must first establish a "connection"
with the ClearPass server. While the ClearPass API is RESTful and uses HTTPS,
there is some setup to be done with regards to authentication. Also, this
connection will be used as a handler to talk with the server later.

To establish a connection with the server, the following information is
required:

:*server hostname*:
    This is the hostname of the server you wish to connect to.

:*client ID*:
    This is basically the username to access the server.

:*client secret*:
    String of random characters from the server admin

:*grant type*:
    This is most likely going to be "client_credentials"

:*ca_certs*:
    This is the filename of a CA cert bundle to be used for validating the
    cert from the server. If not specified, then the defailt Python CA bundles
    will be used.

With this information, a connection can be established:

.. code-block:: python

    import UPennClearPassAPI

    server="cppm-example.isc.upenn.edu"
    client_id="example_user"
    client_secret="FmpeImMzp1GLaMJpxjDSs0aGrMu2cRYd8gKsVCyX4OiS"
    grant_type="client_credentials"

    connection = UPennClearPassAPI.Connection(server, client_id,
                                              client_secret, grant_type)


Establishing a connection could raise the following exceptions:

:AccesTokenError:
    Caused by issues trying to obtain a token. Check to make sure all of
    your information is correct. If continued issues, please reach out to
    the ClearPass Team.

With this connection, we can move on to actually interacting the ClearPass.

Retrieve Endpoint Metadata
--------------------------
The metadata for an endpoint can be easily retrieved from the ClearPass
server.

Example:

.. code-block:: python

    >>> endpoints = UPennClearPassAPI.Endpoints(connection)
    >>> rv = endpoints.get("ff:ff:ff:ff:ff:ff")
    >>> type(rv)
    <class 'dict'>
    >>> print(rv)
    {'_links': {'self': {'href': 'https://cppm-lab-01.isc.upenn.edu/api/endpoint/3033'}},
     'attributes': {},
     'id': 3033,
     'mac_address': 'ffffffffffff',
     'status': 'Unknown'}

Possible exceptions which could be raised:

:UnknownDeviceError:
    If the MAC address for the endpoint doesn't exist on the ClearPass
    server, then this will be raised. Please check the MAC address
    provided.

:UnauthorizedError:
    This is raised when the Client ID is not authorized to complete the
    action. Please contact the ClearPass team to check permissions for the
    client ID.

:UnknownError:
    If something unexpected happens, this will be raised containing the
    an error message, most likely from the server. Please consult the
    ClearPass team if you are unable to resolve the issue.


Disabling an Endpoint
---------------------
When there is a need to disable a endpoint, the following information is
required:

:MAC Address:
    This is the MAC address of the endpoint which is to be disabled. It
    should be in the format of either "aa-bb-cc-dd-ee-ff" or
    "aa:bb:cc:dd:ee:ff". Case doesn't matter.

:Reason for Disabling:
    This should be a brief string explaining why the endpoint is being
    disabled.

:Reference Information:
    This should be a reference to a ticket in some ticketing system,
    preferably Remedy.

With this information, we can disable the endpoint. Using an established
connection, a endpoint can be disabled via the following:

.. code-block:: python

    endpoints = UPennClearPassAPI.Endpoints(connection)

    mac = "aa:bb:cc:dd:ee:ff"
    reason = "this is an example"
    ref = "remedy INC00000000000"

    rV = endpoints.disable(mac, reason, ref)

``UPennClearPassAPI.Endpoints.disable`` will return a dictionary with the
metadata of the endpoint following the disabling of the endpoint. The
dictionary contain the following information:

.. code-block:: python

    >>> print(rv)
    {'_links': {'self': {'href': 'https://cppm-example.isc.upenn.edu/api/endpoint/3031'}},
     'attributes': {'disabled_by': 'example_user',
                'disabled_for': 'the is an example',
                'disabled_on': '2020-09-03T14:17:49.340597-04:00',
                'disabled_ref': 'remedy INC00000000'},
     'id': 3031,
     'mac_address': 'aabbccddeeff',
     'status': 'Disabled'}

When an endpoint is disabled, the ``status`` field of the endpoint is changed
from "Unknown" to "Disabled". Along with that, a number of attributes are
added to the endpoint containing the client ID; date and time; reason for
disabling; and reference information. This information can be retrieved with
``UPennClearPassAPI.Endpoints.get`` or via the **Attributes** tab for the
endpoint in the WebUI.

If there is trouble disabling an endpoint, one of the following exceptions
will be raised:

:UnknownDeviceError:
    If the MAC address for the endpoint doesn't exist on the ClearPass
    server, then this will be raised. Please check the MAC address
    provided.

:UnauthorizedError:
    This is raised when the Client ID is not authorized to complete the
    action. Please contact the ClearPass team to check permissions for the
    client ID.

:UnknownError:
    If something unexpected happens, this will be raised containing the
    an error message, most likely from the server. Please consult the
    ClearPass team if you are unable to resolve the issue.

Enabling an Endpoint
--------------------
When it's time to enable an endpoint, only the MAC address is needed. The
following will re-enable an endpoint:

.. code-block:: python

    endpoints = UPennClearPassAPI.Endpoints(connection)

    mac = "aa:bb:cc:dd:ee:ff"

    rV = endpoints.enable(mac)

``UPennClearPassAPI.Endpoints.enable`` will return a dictionary with the
metadata of the endpoint following the disabling of the endpoint. The
dictionary will contain at least the following information:

.. code-block:: python

    >>> print(rv)
    {'_links': {'self': {'href': 'https://cppm-example.isc.upenn.edu/api/endpoint/3031'}},
     'attributes': {'disabled_by': 'example_user',
                    'disabled_for': 'this is an example',
                    'disabled_on': '2020-09-03T14:17:49.340597-04:00',
                    'disabled_ref': 'remedy INC00000000',
                    'enabled_by': 'example_user',
                    'enabled_on': '2020-09-03T14:21:13.554578-04:00'},
     'id': 3031,
     'mac_address': 'ffffffffffff',
     'status': 'Unknown'}

When an endpoint is enabled, the ``status`` field of the endpoint is changed
from "Disabled" to "Unknown". The client ID and the date/time are added to the
attributes list for the endpoint.

If there is trouble, the following exceptions might be raised:

:UnknownDeviceError:
    If the MAC address for the endpoint doesn't exist on the ClearPass
    server, then this will be raised. Please check the MAC address
    provided.

:UnauthorizedError:
    This is raised when the Client ID is not authorized to complete the
    action. Please contact the ClearPass team to check permissions for the
    client ID.

:UnknownError:
    If something unexpected happens, this will be raised containing the
    an error message, most likely from the server. Please consult the
    ClearPass team if you are unable to resolve the issue.

Deleting an Endpoint
--------------------
When a endpoint is deleted, it creates a state as if the endpoint had never
authenticated via ClearPass before. If the endpoint was disabled, it will
effectly enable the endpoint.


.. code-block:: python

    endpoints = UPennClearPassAPI.Endpoints(connection)

    mac = "aa:bb:cc:dd:ee:ff"

    endpoints.delete(mac)

There is no return from the ``delete`` function.

If there is trouble, the following exceptions might be raised:

:UnknownDeviceError:
    If the MAC address for the endpoint doesn't exist on the ClearPass
    server, then this will be raised. Please check the MAC address
    provided.

:UnauthorizedError:
    This is raised when the Client ID is not authorized to complete the
    action. Please contact the ClearPass team to check permissions for the
    client ID.

:UnknownError:
    If something unexpected happens, this will be raised containing the
    an error message, most likely from the server. Please consult the
    ClearPass team if you are unable to resolve the issue.


Testing
-------
There is a hobbled together script (``test.py``) which currently runs through
all of the test cases for the ``UPennClearPassAPI.Endpoints`` and
``UPennClearPassAPI.Connection``. This will eventally be expanded, and put
into a proper testing framework, but for now, there is this.

Setup
+++++
The testing script makes the assumption the MAC addresses
``ff:ff:ff:ff:ff:ff`` and ``ee:ee:ee:ee:ee:ee`` do not exist on the
ClearPass server.

There should be a privilaged and unprivlaged client ID provisioned for
testing. All of the client and server information is stored in the following
environment variables:

CLEARPASSAPI_TEST_SERVER
    server hostname
CLEARPASSAPI_UNPRIV_USERNAME
    unprivlaged client ID
CLEARPASSAPI_UNPRIV_SECRET
    unprivlaged client secret
CLEARPASSAPI_PRIV_USERNAME
    privlaged client ID
CLEARPASSAPI_PRIV_SECRET
    privlaged client secret

Running Test Script
+++++++++++++++++++
The test script can be run with the following:

.. code-block:: bash

    # python3 test.py
