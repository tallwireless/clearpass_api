import json
import urllib3
import arrow

GET = "GET"
POST = "POST"
DELETE = "DELETE"
PATCH = "PATCH"


class AccessTokenError(Exception):
    """ Error in obtaining an OAuth token from the ClearPass server"""

    pass


class UnauthorizedError(Exception):
    """ Action to the ClearPass server is unauthorized or forbidden"""

    pass


class UnknownError(Exception):
    """ A, you know, unknown error. """

    pass


class UnknownDeviceGroupError(Exception):
    """ Device not found on the ClearPass server"""

    pass


class UnknownDeviceError(Exception):
    """ Device not found on the ClearPass server"""

    pass


class UnknownEndpointError(Exception):
    """ Endpoint not found on the ClearPass server"""

    pass


class InvalidGroupTypeError(Exception):
    """ When the type of device list doesn't match the group type"""

    pass


class DuplicateEndpointError(Exception):
    """When an endpoint already exists"""

    pass


class InvalidDeviceListError(Exception):
    """ When the type of device list doesn't match the group type"""

    pass


class Connection(object):
    """ A connection to the ClearPass Server
    This is connection handler to be used by other ClearPass API objects to be
    able interact with the ClearPass server.
    """

    def __init__(self, server, grant, client_id, client_secret, ca_certs=None):
        """
        Args:
            server (str): hostname or IP address of the ClearPass Server
            grant (str): The type of grant to be used for the OAuth process
                Valid options: password, client_credentials
            client_id (str): Username to access the server
            client_secret (str): shared secret to obtain an access token
        """

        self.baseurl = f"https://{server}/api"
        self.auth_grant_type = grant
        self.auth_client_id = client_id
        self.auth_client_secret = client_secret

        self._request_handler = None

        if ca_certs is None:
            self._request_handler = urllib3.PoolManager()
        else:
            self._request_handler = urllib3.PoolManager(
                cert_reqs="CERT_REQUIRED", ca_certs=ca_certs
            )

        self.token_access = None
        self.token_type = None
        self.token_expires = None
        self.token_scope = None

        self.__get_access_token()

    def __get_access_token(self):
        """Get OAuth 2.0 access token with provided parameters

        Based on https://github.com/aruba/clearpass-api-python-snippets

        Note:
            This should not be called directly outside of the Connection
            class.
        """

        url = "{baseurl}/oauth".format(baseurl=self.baseurl)

        headers = {"Content-Type": "application/json", "Accept": "application/json"}

        payload = {
            "grant_type": self.auth_grant_type,
            "client_id": self.auth_client_id,
            "client_secret": self.auth_client_secret,
        }

        r = self._request_handler.request(
            "POST", url, headers=headers, body=json.dumps(payload).encode("utf-8")
        )

        response = json.loads(r.data.decode("utf-8"))
        if r.status != 200:
            raise AccessTokenError("Error getting access token from server")

        # Update the local information with the responses
        self.token_access = response["access_token"]
        self.token_type = response["token_type"]
        self.token_expires = response["expires_in"]
        self.token_scope = response["scope"]

    def call_api(self, api, method, params=None, payload=None):
        """ Make an API call to the ClearPass server

        This is a generic method to make a API call to the ClearPass server

        Args:
            api (str): This is the rest of the API url to be called.
                This string will be appended to the end of the self.base_url.
                The base URL is in the format "https://{server}/api. The api
                argument should include the leading '/'.
            method (const): HTTP method to use for the API call.
                Valid values are: GET, POST, PATCH, PUT, DELETE
            params (dict, optional): URL string parameters
            payload (dict, optional): payload to be sent in the call.
                This dict will be converted to a JSON string before being
                sent.

        Returns:
            Response object from urllib3 call

        Raises:
                UnauthorizedError: API token is not authorized to complete the
                    requested action.
        """

        headers = {
            "Accept": "application/json",
            "Content-type": "application/json",
            "Authorization": "{} {}".format(self.token_type, self.token_access),
        }

        url = "{baseurl}{api}".format(baseurl=self.baseurl, api=api)

        response = self._request_handler.request(
            method,
            url,
            headers=headers,
            fields=params,
            body=json.dumps(payload).encode("utf-8"),
        )

        # check to see if the response was either unauthorized or forbidden
        if response.status in [401, 403]:
            raise UnauthorizedError()

        return response


class Endpoints(object):
    """ Interface to manage user endpoints in ClearPass
    """

    def __init__(self, connection):
        """
        Args:
            connection (`Connection`): Connection handler for ClearPass
        """
        self.connection = connection

    def get(self, mac_address):
        """ Retrieve an endpoint

        Args:
            mac_address (str): MAC address of endpoint to retrieve from
                ClearPass Server
        Returns:
            dict: the data about the device
        Raises:
            UnknownEndpointError: Unable to find device on ClearPass Server
        """
        api = f"/endpoint/mac-address/{mac_address}"
        response = self.connection.call_api(api, GET)

        # Unable to find the device
        if response.status == 404:
            raise UnknownEndpointError()

        return json.loads(response.data)

    def disable(self, mac_address, reason, reference):
        """ Disables an endpoint

        This will mark a endpoint as disabled within ClearPass forcing the
        device into a captive portal informing the user the device has been
        disabled. A reason for being disabled along with a reference must be
        provided. The date/time will be auto filled in.

        Args:
            mac_address (str): MAC address to be disabled
            reason (str): Reason for being disabled
            reference (str): Reference information (i.e. ticket number)
        Returns:
            dict: Response from the server containing update information on
                the device
        Raises:
            UnknownEndpointError: Device isn't found
        """

        api = f"/endpoint/mac-address/{mac_address}"
        attrib = {
            "status": "Disabled",
            "attributes": {
                "disabled_by": self.connection.auth_client_id,
                "disabled_on": str(arrow.now()),
                "disabled_ref": reference,
                "disabled_for": reason,
            },
        }

        response = self.connection.call_api(api, PATCH, payload=attrib)

        if response.status == 404:
            raise UnknownEndpointError()
        if response.status != 200:
            raise UnknownError(f"{response.status}: {response.data}")
        return json.loads(response.data)

    def enable(self, mac_address):
        """ Enables an endpoint

        This will mark the endpoint as enabled in ClearPass allowing the
        endpoint to connect to the wireless network.

        Args:
            mac_address (str): MAC address to be enabled
        Returns:
            dict: Response from the server containing update information on
                the device
        Raises:
            UnknownEndpointError: Device isn't found
        """
        api = f"/endpoint/mac-address/{mac_address}"
        attrib = {
            "status": "Unknown",
            "attributes": {
                "enabled_by": self.connection.auth_client_id,
                "enabled_on": str(arrow.now()),
            },
        }

        response = self.connection.call_api(api, PATCH, payload=attrib)

        if response.status == 404:
            raise UnknownEndpointError()
        if response.status != 200:
            raise UnknownError(f"{response.status}: {response.data}")

        return json.loads(response.data)

    def add(self, mac_address):
        """ Adds an endpoint to ClearPass

        Adds an endpoint to ClearPass. The endpoint will be added in an
        enabled state. If there is a need to disable the node, use
        `disable()`.

        Args:
            mac_address (str): MAC address to be added
        Returns:
            dict: Response from the server containing update information on
                the device
        Raises:
            DuplicateEndpointError: The endpoint already exists on the
                ClearPass server
            UnknownError: There was an unknown error while creating the
                endpoint

        """
        api = "/endpoint"
        attrib = {}
        attrib["mac_address"] = mac_address
        attrib["status"] = "Unknown"
        response = self.connection.call_api(api, POST, payload=attrib)
        # Error out on anything but success
        if response.status == 422:
            raise DuplicateEndpointError("Endpoint Exists")
        if response.status != 201:
            raise UnknownError(f"{response.status}: {response.data}")
        return json.loads(response.data)

    def delete(self, mac_address):
        """ deletes an endpoint to ClearPass

        Deletes an endpoint from ClearPass. This effectively enables the
        endpoint and removes all metadata associated with the endpoint.
        Doesn't have any affect on ClearPass Guest (AirPennNet-Device)
        registrations.

        Args:
            mac_address (str): MAC address to be added
        Raises:
            UnknownEndpointError: Device isn't found
            UnknownError: There was an unknown error while creating the
                endpoint

        """
        api = f"/endpoint/mac-address/{mac_address}"
        response = self.connection.call_api(api, DELETE)
        # Error out on anything but success
        if response.status == 404:
            raise UnknownEndpointError()
        if response.status != 204:
            raise UnknownError(f"{response.status}: {response.data}")
        return


class Devices(object):
    """ Interface to manage user endpoints in ClearPass
    """

    def __init__(self, connection):
        """
        Args:
            connection (`Connection`): Connection handler for ClearPass
        """
        self.connection = connection

    def get_devices(self, filter={}):
        """ Get a list of network devices from ClearPass

        This will grab a list of devices from ClearPass and return it in a
        list of dictionaries. Only the first 1000 entries will be returned.

        Args:
            filter (dict, optional): dictionary describing a JSON filter set

        Returns:
            list of dictionaries: each dictionary will have the information
                about the network device
        Raises:
            UnknownError: Something goes wrong
        """
        params = {"limit": 1000, "filter": json.dumps(filter)}
        response = self.connection.call_api("/network-device", GET, params=params)

        if response.status != 200:
            error_details = json.loads(response.data)["details"]
            raise UnknownError(f"{response.status}: {error_details}")

        return json.loads(response.data)["_embedded"]["items"]

    def delete_device(self, id):
        """ Delete network device out of ClearPass

        This remotes a network device from ClearPass. Removing a device will
        cause the network device to no longer be able to do authentications
        against ClearPass.

        Args:
            id (int): ID number off the network device

        Raises:
            UnknownDeviceError: Unable to find the given ID number
            UnknownError: Something happen preventing the action to happen
        """

        response = self.connection.call_api(f"/network-device/{id}", DELETE)
        if response.status == 404:
            raise UnknownDeviceError()
        if response.status != 204:
            error_details = json.loads(response.data)["details"]
            raise UnknownError(f"{response.status}: {error_details}")

    def get_device(self, id):
        """ Get a network device

        This will return the information about a network device.

        Args:
            id (int): ID  number of the network device

        Returns:
            dict: contains metadata about the device

        Raises:
            UnknownDeviceError: Unable to find given device
            UnknownError: Failed for an unknown reason
        """
        response = self.connection.call_api(f"/network-device/{id}", GET)

        if response.status == 404:
            raise UnknownDeviceError()
        if response.status != 200:
            error_details = json.loads(response.data)["details"]
            raise UnknownError(f"{response.status}: {error_details}")
        return json.loads(response.data)["_embedded"]["items"]


class DeviceGroups(object):
    """ Interface to manage user endpoints in ClearPass
    """

    def __init__(self, connection):
        """
        Args:
            connection (`Connection`): Connection handler for ClearPass
        """
        self.connection = connection

    def get(self, name=None, filter={}):
        """ Get a list of device groups

        This will grab a list of device groups from ClearPass and return it in
        a list of dictionaries. Only the first 1000 entries will be returned.

        Args:
            name (str, optional): name of group, if not provided, first 1000
                entries will be returned
            filter (dict, optional): JSON filter for request, ignored if name
                provided

        Returns:
            list of dictonaries: contains dictionaries of device group
                metadata

        Raises:
            UnknownError: Failed for an unknown reason
        """
        if name is None:
            url = "/network-device-group"
            params = {"limit": 1000, "filter": json.dumps(filter)}
        else:
            url = "/network-device-group/name/{name}"
            params = {}

        response = self.connection.call_api(url, GET, params=params)
        if response.status == 404:
            raise UnknownDeviceGroupError()

        if response.status != 200:
            error_details = json.loads(response.data)["details"]
            raise UnknownError(f"{response.status}: {error_details}")

        return json.loads(response.data)["_embedded"]["items"]

    def update(self, group_name, device_list, group_format="list"):
        """ Update an existing network device group

        This will update an existing network device group.

        There are three different formats the list of device_list:

            subnet: a single IPv4 subnet
            regex: a regular experssion performed on the IP address of the
                device
            list: list of IP addresses for the devices

        Args:
            group_name (str): name of the group
            device_list (list or str): definition of the  group
            group_format (str): the type for the device_list.
                Valid options: list (default), subnet, regex

        Returns:
            dict: contains updated information from server

        Raises:
            UnknownDeviceGroupError: Unable to find the Device Group on the
                server
            UnknownError: Failed for an unknown reason
            InvalidDeviceListError: Defined list is of the wrong type
            InvalidGroupTypeError: Invalid group type provided
        """
        group_format = group_format.lower()
        # check to make sure we have a vaild group_format and device_list
        if group_format not in ["subnet", "list", "regex"]:
            raise InvalidGroupTypeError("Unknown group type")

        if group_format in ["subnet", "regex"] and device_list is not str:
            raise InvalidDeviceListError(
                f"device_list must be a str for {group_format}"
            )

        if group_format in ["list"] and device_list is not list:
            raise InvalidDeviceListError(
                f"device_list must be a list of str for {group_format}"
            )

        api = f"/network-device-group/name/{group_name}"

        attrib = {}
        attrib = {"group_format": group_format, "value": device_list}

        response = self.connection.call_api(api, PATCH, payload=attrib)

        if response.status == 404:
            raise UnknownDeviceGroupError()

        if response.status in [406, 415, 422]:
            error_details = json.loads(response.data)["details"]
            raise UnknownError(f"{response.status}: {error_details}")

        return json.loads(response.data)["_embedded"]["items"]

    def add(self, group_name, device_list, group_format="list"):
        """ Add network device group

        Add a new network device group

        There are three different formats the list of device_list:

            subnet: a single IPv4 subnet
            regex: a regular experssion performed on the IP address of the
                device
            list: list of IP addresses for the devices

        Args:
            group_name (str): name of the group
            device_list (list or str): definition of the  group
            group_format (str): the type for the device_list.
                Valid options: list (default), subnet, regex

        Returns:
            dict: contains updated information from server

        Raises:
            UnknownError: Failed for an unknown reason
            InvalidDeviceListError: Defined list is of the wrong type
            InvalidGroupTypeError: Invalid group type provided
        """
        group_format = group_format.lower()
        # check to make sure we have a vaild group_format and device_list
        if group_format not in ["subnet", "list", "regex"]:
            raise InvalidGroupTypeError("Unknown group type")

        if group_format in ["subnet", "regex"] and device_list is not str:
            raise InvalidDeviceListError(
                f"device_list must be a str for {group_format}"
            )

        if group_format in ["list"] and device_list is not list:
            raise InvalidDeviceListError(
                f"device_list must be a list of str for {group_format}"
            )

        attrib = {}
        attrib["value"] = device_list
        attrib["group_format"] = group_format
        attrib["name"] = group_name
        api = "/network-device-group"
        response = self.connection.call_api(api, POST, payload=attrib)

        if response.status != 200:
            error_details = json.loads(response.data)["details"]
            raise UnknownError(f"{response.status}: {error_details}")

        return response
